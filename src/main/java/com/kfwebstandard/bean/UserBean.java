package com.kfwebstandard.bean;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

@Named("user")
@SessionScoped
public class UserBean implements Serializable {

    // Default logger is java.util.logging
    private static final Logger LOG = Logger.getLogger("UserBean.class");

    private String name;
    private String password;
    private String aboutYourself;

    public String getName() {
        LOG.info("UserBean: getName");
        return name;
    }

    public void setName(String newValue) {
        LOG.info("UserBean: setName");
        name = newValue;
    }

    public String getPassword() {
        LOG.info("UserBean: getPassword");
        return password;
    }

    public void setPassword(String newValue) {
        LOG.info("UserBean: setPassword");
        password = newValue;
    }

    public String getAboutYourself() {
        LOG.info("UserBean: getAboutyourself");
        return aboutYourself;
    }

    public void setAboutYourself(String newValue) {
        LOG.info("UserBean: setAboutyourself");
        aboutYourself = newValue;
    }
}
