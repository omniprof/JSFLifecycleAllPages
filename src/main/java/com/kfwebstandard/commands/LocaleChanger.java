package com.kfwebstandard.commands;

import java.io.Serializable;
import java.util.Locale;
import java.util.logging.Logger;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;

/**
 * Change the locale and redisplay the tree in the new language
 *
 * @author Ken Fogel
 */
@Named
@SessionScoped
public class LocaleChanger implements Serializable {

    // Default logger is java.util.logging
    private static final Logger LOG = Logger.getLogger("UserBean.class");

    public String frenchAction() {
        LOG.info("frenchAction");
        FacesContext context = FacesContext.getCurrentInstance();
        context.getViewRoot().setLocale(Locale.CANADA_FRENCH);
        return null;
    }

    public String englishAction() {
        LOG.info("englishAction");
        FacesContext context = FacesContext.getCurrentInstance();
        context.getViewRoot().setLocale(Locale.CANADA);
        return null;
    }
}
