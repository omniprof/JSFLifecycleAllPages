package com.kfwebstandard.listener;

import java.util.logging.Logger;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

/**
 * This class will intercept every JSF phase and display the phase ID
 *
 * @author Ken Fogel
 */
public class LifeCycleListener implements PhaseListener {

    // Default logger is java.util.logging
    private static final Logger LOG = Logger.getLogger("LifeCycleListener.class");

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.ANY_PHASE;
    }

    @Override
    public void beforePhase(PhaseEvent event) {
        LOG.info("-----------------------------------");
        LOG.info("Start Phase: " + event.getPhaseId());
    }

    @Override
    public void afterPhase(PhaseEvent event) {
        LOG.info("End Phase: " + event.getPhaseId());
    }
}
